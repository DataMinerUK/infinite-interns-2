Infinite Interns 2
==================

Docker Containers For Data Driven Investigations

This repository contains Docker containers. You may be more interested in using the skel2 project for accessing these containers.

Available Interns
-----------------

* `twinejs`: A web-based tool for interactive fiction.

How To Build Interns
------------------------

The build process is similar for each intern, here we use the `twinejs` interns as an example.

	cd twinejs
	docker build --rm=true -t datamineruk/twinejs .

To run the intern use

	docker run -i -t -p 80:80 datamineruk/twinejs


How To Work With Docker
-----------------------

To get onto the intern

	docker run -i -t datamineruk/twinejs /bin/bash

